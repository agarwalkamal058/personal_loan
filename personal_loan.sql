-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: personal_loan
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_customermaster`
--

DROP TABLE IF EXISTS `app_customermaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_customermaster` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) NOT NULL,
  `phone_number` bigint(20) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `dob` datetime(6) DEFAULT NULL,
  `yearly_income` bigint(20) NOT NULL,
  `occupation` varchar(20) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `aadhaar_no` bigint(20) DEFAULT NULL,
  `pancard_no` varchar(12) DEFAULT NULL,
  `passport` varchar(20) DEFAULT NULL,
  `created_on` datetime(6) NOT NULL,
  `updated_on` datetime(6) NOT NULL,
  `created_by_id` varchar(20) DEFAULT NULL,
  `updated_by_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `app_customermaster_created_by_id_7c6a958f_fk_app_userm` (`created_by_id`),
  KEY `app_customermaster_updated_by_id_7ee1166d_fk_app_userm` (`updated_by_id`),
  CONSTRAINT `app_customermaster_created_by_id_7c6a958f_fk_app_userm` FOREIGN KEY (`created_by_id`) REFERENCES `app_usermaster` (`emp_id`),
  CONSTRAINT `app_customermaster_updated_by_id_7ee1166d_fk_app_userm` FOREIGN KEY (`updated_by_id`) REFERENCES `app_usermaster` (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_customermaster`
--

LOCK TABLES `app_customermaster` WRITE;
/*!40000 ALTER TABLE `app_customermaster` DISABLE KEYS */;
INSERT INTO `app_customermaster` VALUES (1,'Kamal Agarwal',8005460659,'kamal@ezeiatech.in','2020-01-23 10:50:30.000000',800000,'SERVICE','SHAHJAHANPUR',359865865845,'BNG56566H89','TN7879898HJ89','2020-01-23 10:50:55.513024','2020-01-23 10:50:55.513065','SUPERUSER','SUPERUSER'),(2,'Surendra Sharma',9650960774,'surender@gmail.com','2014-03-05 18:30:00.000000',96000,'SERVICE','Gorakhpur',733687862535,'BNG56566H89','T','2020-01-23 11:03:38.894135','2020-01-23 11:03:38.894268',NULL,NULL),(3,'Surendra Sharma',9650960774,'surender@gmail.com','2014-03-05 18:30:00.000000',96000,'SERVICE','Gorakhpur',733687862535,'BNG56566H89','TN7879898HJ90','2020-01-23 11:04:04.236353','2020-01-23 11:04:04.236379',NULL,NULL),(4,'Surendra Sharma',9650960774,'surender@gmail.com','2014-03-05 18:30:00.000000',96000,'SERVICE','Gorakhpur',733687862535,'BNG56566H89','TN7879898HJ90','2020-01-23 11:06:42.715343','2020-01-23 11:06:42.715365',NULL,NULL),(5,'Surendra Sharma',9650960774,'surender@gmail.com','2014-03-05 18:30:00.000000',96000,'SERVICE','Gorakhpur',733687862535,'BNG56566H89','TN7879898HJ90','2020-01-23 11:07:15.411250','2020-01-23 11:07:15.411269',NULL,NULL),(6,'Vaibhav',9650960774,'vaibhav@gmail.com','1990-02-04 18:30:00.000000',850000,'SERVICE','Gorakhpur',733633363336,'454545465465','JGHHJNB24514','2020-01-23 12:24:01.914029','2020-01-23 12:24:01.914057',NULL,NULL);
/*!40000 ALTER TABLE `app_customermaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_designationmaster`
--

DROP TABLE IF EXISTS `app_designationmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_designationmaster` (
  `designation` varchar(20) NOT NULL,
  `designation_name` varchar(30) DEFAULT NULL,
  `priority` smallint(5) unsigned NOT NULL,
  `created_on` datetime(6) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  PRIMARY KEY (`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_designationmaster`
--

LOCK TABLES `app_designationmaster` WRITE;
/*!40000 ALTER TABLE `app_designationmaster` DISABLE KEYS */;
INSERT INTO `app_designationmaster` VALUES ('MANAGER','Manager',8,'2020-01-23 06:19:55.437342','SUPERUSER'),('SUPERUSER','Super User',10,'2020-01-23 06:12:35.966607','SUPERUSER');
/*!40000 ALTER TABLE `app_designationmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_loanmaster`
--

DROP TABLE IF EXISTS `app_loanmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_loanmaster` (
  `auto_display_id` int(11) NOT NULL,
  `loan_acc_no` varchar(50) NOT NULL,
  `loan_start_date` datetime(6) NOT NULL,
  `loan_end_date` datetime(6) DEFAULT NULL,
  `loan_amount` double NOT NULL,
  `created_on` datetime(6) NOT NULL,
  `updated_on` datetime(6) NOT NULL,
  `created_by_id` varchar(20) DEFAULT NULL,
  `loan_scheme_id` int(11) DEFAULT NULL,
  `updated_by_id` varchar(20) DEFAULT NULL,
  `loan_status` varchar(12) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`loan_acc_no`),
  KEY `app_loanmaster_created_by_id_573b6f0c_fk_app_usermaster_emp_id` (`created_by_id`),
  KEY `app_loanmaster_loan_scheme_id_666a8b60_fk_app_loans` (`loan_scheme_id`),
  KEY `app_loanmaster_updated_by_id_21a6b766_fk_app_usermaster_emp_id` (`updated_by_id`),
  KEY `app_loanmaster_customer_id_e6424de9_fk_app_custo` (`customer_id`),
  CONSTRAINT `app_loanmaster_created_by_id_573b6f0c_fk_app_usermaster_emp_id` FOREIGN KEY (`created_by_id`) REFERENCES `app_usermaster` (`emp_id`),
  CONSTRAINT `app_loanmaster_customer_id_e6424de9_fk_app_custo` FOREIGN KEY (`customer_id`) REFERENCES `app_customermaster` (`customer_id`),
  CONSTRAINT `app_loanmaster_loan_scheme_id_666a8b60_fk_app_loans` FOREIGN KEY (`loan_scheme_id`) REFERENCES `app_loanschememaster` (`loan_scheme_id`),
  CONSTRAINT `app_loanmaster_updated_by_id_21a6b766_fk_app_usermaster_emp_id` FOREIGN KEY (`updated_by_id`) REFERENCES `app_usermaster` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_loanmaster`
--

LOCK TABLES `app_loanmaster` WRITE;
/*!40000 ALTER TABLE `app_loanmaster` DISABLE KEYS */;
INSERT INTO `app_loanmaster` VALUES (1,'ACLN1000000001','2020-01-23 09:40:13.000000','2025-01-23 09:40:19.000000',5000000,'2020-01-23 09:41:05.478412','2020-01-23 09:41:05.478423','EMP29112019',2,'EMP29112019','APPROVED',NULL),(2,'ACLN1000000002','2020-05-04 18:30:00.000000','2039-05-04 18:30:00.000000',36500000,'2020-01-23 12:28:41.735230','2020-01-23 12:28:41.735247',NULL,2,NULL,'REJECTED',4),(3,'ACLN1000000003','2020-01-02 18:30:00.000000','2029-01-15 18:30:00.000000',6500000,'2020-01-23 15:37:34.974237','2020-01-23 15:37:34.974263',NULL,2,NULL,'APPROVED',1),(4,'ACLN1000000004','2020-01-02 18:30:00.000000','2029-01-15 18:30:00.000000',6500000,'2020-01-23 15:40:48.872026','2020-01-23 15:40:48.872043',NULL,2,NULL,'APPROVED',1),(5,'ACLN1000000005','2020-01-02 18:30:00.000000','2029-01-15 18:30:00.000000',6500000,'2020-01-23 15:41:04.603340','2020-01-23 15:41:04.603354',NULL,2,NULL,'REJECTED',1),(6,'ACLN1000000006','2020-01-02 18:30:00.000000','2029-01-15 18:30:00.000000',6500000,'2020-01-23 15:41:19.353044','2020-01-23 15:41:19.353055',NULL,2,NULL,'PENDING',1),(7,'ACLN1000000007','2020-01-02 18:30:00.000000','2029-01-15 18:30:00.000000',6500000,'2020-01-23 15:42:01.580021','2020-01-23 15:42:01.580034',NULL,2,NULL,'PENDING',1),(8,'ACLN1000000008','2020-01-02 18:30:00.000000','2029-01-15 18:30:00.000000',6500000,'2020-01-23 15:44:22.263234','2020-01-23 15:44:22.263265',NULL,2,NULL,'PENDING',1);
/*!40000 ALTER TABLE `app_loanmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_loanschememaster`
--

DROP TABLE IF EXISTS `app_loanschememaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_loanschememaster` (
  `loan_scheme_id` int(11) NOT NULL AUTO_INCREMENT,
  `loan_type` varchar(20) NOT NULL,
  `content` longtext NOT NULL,
  `amount_range` varchar(50) NOT NULL,
  `scheme_years` smallint(5) unsigned NOT NULL,
  `scheme_months` smallint(5) unsigned NOT NULL,
  `interest_rate` double NOT NULL,
  `created_on` datetime(6) NOT NULL,
  `updated_on` datetime(6) NOT NULL,
  `created_by_id` varchar(20) DEFAULT NULL,
  `updated_by_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`loan_scheme_id`),
  KEY `app_loanschememaster_created_by_id_50b97361_fk_app_userm` (`created_by_id`),
  KEY `app_loanschememaster_updated_by_id_822b9e46_fk_app_userm` (`updated_by_id`),
  CONSTRAINT `app_loanschememaster_created_by_id_50b97361_fk_app_userm` FOREIGN KEY (`created_by_id`) REFERENCES `app_usermaster` (`emp_id`),
  CONSTRAINT `app_loanschememaster_updated_by_id_822b9e46_fk_app_userm` FOREIGN KEY (`updated_by_id`) REFERENCES `app_usermaster` (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_loanschememaster`
--

LOCK TABLES `app_loanschememaster` WRITE;
/*!40000 ALTER TABLE `app_loanschememaster` DISABLE KEYS */;
INSERT INTO `app_loanschememaster` VALUES (2,'PERSONAL','For the personal use only','BELOWONELAKH',5,0,14,'2020-01-23 09:24:35.705407','2020-01-23 09:24:35.705471','EMP29112019','EMP29112019');
/*!40000 ALTER TABLE `app_loanschememaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_paymentmaster`
--

DROP TABLE IF EXISTS `app_paymentmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_paymentmaster` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(120) NOT NULL,
  `amount` double NOT NULL,
  `payment_status` varchar(12) NOT NULL,
  `created_on` datetime(6) NOT NULL,
  `updated_on` datetime(6) NOT NULL,
  `loan_acc_no_id` varchar(50) NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `app_paymentmaster_loan_acc_no_id_f71bb8eb_fk_app_loanm` (`loan_acc_no_id`),
  CONSTRAINT `app_paymentmaster_loan_acc_no_id_f71bb8eb_fk_app_loanm` FOREIGN KEY (`loan_acc_no_id`) REFERENCES `app_loanmaster` (`loan_acc_no`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_paymentmaster`
--

LOCK TABLES `app_paymentmaster` WRITE;
/*!40000 ALTER TABLE `app_paymentmaster` DISABLE KEYS */;
INSERT INTO `app_paymentmaster` VALUES (1,'Temp',10000,'SUCCESS','2020-01-23 10:31:47.435854','2020-01-23 10:31:47.435925','ACLN1000000001'),(2,'New Payment',30000,'SUCCESS','2020-01-23 11:53:18.726381','2020-01-23 11:53:18.726602','ACLN1000000001'),(3,'Second Payment.',25000,'SUCCESS','2020-01-23 15:12:37.038882','2020-01-23 15:12:37.038929','ACLN1000000001'),(4,'Third Payment',35000,'SUCCESS','2020-01-23 15:28:09.923211','2020-01-23 15:28:09.923260','ACLN1000000001');
/*!40000 ALTER TABLE `app_paymentmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_usermaster`
--

DROP TABLE IF EXISTS `app_usermaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_usermaster` (
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `emp_id` varchar(20) NOT NULL,
  `emp_name` varchar(200) NOT NULL,
  `emp_phone_num` bigint(20) DEFAULT NULL,
  `email_id` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_on` datetime(6) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `updated_on` datetime(6) NOT NULL,
  `updated_by` varchar(200) NOT NULL,
  `designation_id` varchar(20) NOT NULL,
  PRIMARY KEY (`emp_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `emp_phone_num` (`emp_phone_num`),
  KEY `app_usermaster_designation_id_e6aebf83_fk_app_desig` (`designation_id`),
  CONSTRAINT `app_usermaster_designation_id_e6aebf83_fk_app_desig` FOREIGN KEY (`designation_id`) REFERENCES `app_designationmaster` (`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_usermaster`
--

LOCK TABLES `app_usermaster` WRITE;
/*!40000 ALTER TABLE `app_usermaster` DISABLE KEYS */;
INSERT INTO `app_usermaster` VALUES ('pbkdf2_sha256$150000$TEA0fmLJIsFN$U3l+r0tlOUut8xm3uBoz33JAue0SHpqwvLvOTRTy1Mo=',NULL,0,'9650960774','Kamal','Agarwal','agarwalkamal058@gmail.com',1,'2020-01-23 06:20:47.815395','EMP29112019','Kamal Agarwal',9650960774,NULL,1,'2020-01-23 06:20:47.815460','Super User','2020-01-23 06:23:18.974255','Super User','MANAGER'),('pbkdf2_sha256$150000$T9VkHVitexzF$2pVy7V5huOVKHKEa+WyQyV8WKFz4VbrY4J/jCZXfSUs=','2020-01-23 09:22:06.968077',1,'8005460659','','','',1,'2020-01-23 06:14:26.740211','SUPERUSER','Super User',8005460659,'kamalagarwal',1,'2020-01-23 06:14:26.740279','SUPERUSER','2020-01-23 06:14:26.740300','SUPERUSER','SUPERUSER');
/*!40000 ALTER TABLE `app_usermaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_usermaster_groups`
--

DROP TABLE IF EXISTS `app_usermaster_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_usermaster_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usermaster_id` varchar(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_usermaster_groups_usermaster_id_group_id_1b8cfd51_uniq` (`usermaster_id`,`group_id`),
  KEY `app_usermaster_groups_group_id_854e9206_fk_auth_group_id` (`group_id`),
  CONSTRAINT `app_usermaster_group_usermaster_id_5a272bdb_fk_app_userm` FOREIGN KEY (`usermaster_id`) REFERENCES `app_usermaster` (`emp_id`),
  CONSTRAINT `app_usermaster_groups_group_id_854e9206_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_usermaster_groups`
--

LOCK TABLES `app_usermaster_groups` WRITE;
/*!40000 ALTER TABLE `app_usermaster_groups` DISABLE KEYS */;
INSERT INTO `app_usermaster_groups` VALUES (1,'EMP29112019',1);
/*!40000 ALTER TABLE `app_usermaster_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_usermaster_user_permissions`
--

DROP TABLE IF EXISTS `app_usermaster_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_usermaster_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usermaster_id` varchar(20) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_usermaster_user_perm_usermaster_id_permission_ecfe1b3e_uniq` (`usermaster_id`,`permission_id`),
  KEY `app_usermaster_user__permission_id_33fdd29a_fk_auth_perm` (`permission_id`),
  CONSTRAINT `app_usermaster_user__permission_id_33fdd29a_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `app_usermaster_user__usermaster_id_097fcbd2_fk_app_userm` FOREIGN KEY (`usermaster_id`) REFERENCES `app_usermaster` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_usermaster_user_permissions`
--

LOCK TABLES `app_usermaster_user_permissions` WRITE;
/*!40000 ALTER TABLE `app_usermaster_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_usermaster_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'MANAGER_GROUP');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (1,1,1),(2,1,2),(3,1,4),(4,1,5),(5,1,6),(6,1,8),(7,1,13),(8,1,14),(9,1,16),(10,1,17),(11,1,18),(12,1,20),(13,1,21),(14,1,22),(15,1,24),(16,1,25),(17,1,26),(18,1,28),(19,1,29),(20,1,30),(21,1,32);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can view permission',1,'view_permission'),(5,'Can add group',2,'add_group'),(6,'Can change group',2,'change_group'),(7,'Can delete group',2,'delete_group'),(8,'Can view group',2,'view_group'),(9,'Can add content type',3,'add_contenttype'),(10,'Can change content type',3,'change_contenttype'),(11,'Can delete content type',3,'delete_contenttype'),(12,'Can view content type',3,'view_contenttype'),(13,'Can add User Master',4,'add_usermaster'),(14,'Can change User Master',4,'change_usermaster'),(15,'Can delete User Master',4,'delete_usermaster'),(16,'Can view User Master',4,'view_usermaster'),(17,'Can add Designation Master',5,'add_designationmaster'),(18,'Can change Designation Master',5,'change_designationmaster'),(19,'Can delete Designation Master',5,'delete_designationmaster'),(20,'Can view Designation Master',5,'view_designationmaster'),(21,'Can add Loan Scheme Master',6,'add_loanschememaster'),(22,'Can change Loan Scheme Master',6,'change_loanschememaster'),(23,'Can delete Loan Scheme Master',6,'delete_loanschememaster'),(24,'Can view Loan Scheme Master',6,'view_loanschememaster'),(25,'Can add Loan Master',7,'add_loanmaster'),(26,'Can change Loan Master',7,'change_loanmaster'),(27,'Can delete Loan Master',7,'delete_loanmaster'),(28,'Can view Loan Master',7,'view_loanmaster'),(29,'Can add Customer Master',8,'add_customermaster'),(30,'Can change Customer Master',8,'change_customermaster'),(31,'Can delete Customer Master',8,'delete_customermaster'),(32,'Can view Customer Master',8,'view_customermaster'),(33,'Can add log entry',9,'add_logentry'),(34,'Can change log entry',9,'change_logentry'),(35,'Can delete log entry',9,'delete_logentry'),(36,'Can view log entry',9,'view_logentry'),(37,'Can add session',10,'add_session'),(38,'Can change session',10,'change_session'),(39,'Can delete session',10,'delete_session'),(40,'Can view session',10,'view_session'),(41,'Can add Payment Master',11,'add_paymentmaster'),(42,'Can change Payment Master',11,'change_paymentmaster'),(43,'Can delete Payment Master',11,'delete_paymentmaster'),(44,'Can view Payment Master',11,'view_paymentmaster');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_app_usermaster_emp_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_app_usermaster_emp_id` FOREIGN KEY (`user_id`) REFERENCES `app_usermaster` (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2020-01-23 06:19:55.445470','MANAGER','Manager',1,'[{\"added\": {}}]',5,'SUPERUSER'),(2,'2020-01-23 06:20:48.324861','EMP29112019','Kamal Agarwal',1,'[{\"added\": {}}]',4,'SUPERUSER'),(3,'2020-01-23 06:21:31.539025','SUPERUSER','Super User',2,'[{\"changed\": {\"fields\": [\"priority\"]}}]',5,'SUPERUSER'),(4,'2020-01-23 06:23:05.847177','1','MANAGER_GROUP',1,'[{\"added\": {}}]',2,'SUPERUSER'),(5,'2020-01-23 06:23:18.988084','EMP29112019','Kamal Agarwal',2,'[{\"changed\": {\"fields\": [\"groups\"]}}]',4,'SUPERUSER'),(6,'2020-01-23 06:23:43.692813','MANAGER','Manager',2,'[{\"changed\": {\"fields\": [\"priority\"]}}]',5,'SUPERUSER'),(7,'2020-01-23 09:24:35.719199','2','2',1,'[{\"added\": {}}]',6,'SUPERUSER'),(8,'2020-01-23 09:41:05.537355','ACLN1000000001','ACLN1000000001',1,'[{\"added\": {}}]',7,'SUPERUSER'),(9,'2020-01-23 10:31:47.437162','1','1',1,'[{\"added\": {}}]',11,'SUPERUSER'),(10,'2020-01-23 10:50:55.535895','1','1',1,'[{\"added\": {}}]',8,'SUPERUSER');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (9,'admin','logentry'),(8,'app','customermaster'),(5,'app','designationmaster'),(7,'app','loanmaster'),(6,'app','loanschememaster'),(11,'app','paymentmaster'),(4,'app','usermaster'),(2,'auth','group'),(1,'auth','permission'),(3,'contenttypes','contenttype'),(10,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2020-01-23 06:08:14.891738'),(2,'contenttypes','0002_remove_content_type_name','2020-01-23 06:08:17.236236'),(3,'auth','0001_initial','2020-01-23 06:08:19.778332'),(4,'auth','0002_alter_permission_name_max_length','2020-01-23 06:08:33.672569'),(5,'auth','0003_alter_user_email_max_length','2020-01-23 06:08:33.831873'),(6,'auth','0004_alter_user_username_opts','2020-01-23 06:08:33.992349'),(7,'auth','0005_alter_user_last_login_null','2020-01-23 06:08:34.200676'),(8,'auth','0006_require_contenttypes_0002','2020-01-23 06:08:34.275896'),(9,'auth','0007_alter_validators_add_error_messages','2020-01-23 06:08:34.372247'),(10,'auth','0008_alter_user_username_max_length','2020-01-23 06:08:34.793293'),(11,'auth','0009_alter_user_last_name_max_length','2020-01-23 06:08:34.892894'),(12,'auth','0010_alter_group_name_max_length','2020-01-23 06:08:35.281410'),(13,'auth','0011_update_proxy_permissions','2020-01-23 06:08:35.373850'),(14,'app','0001_initial','2020-01-23 06:08:43.614041'),(15,'admin','0001_initial','2020-01-23 06:09:35.077027'),(16,'admin','0002_logentry_remove_auto_add','2020-01-23 06:09:37.784387'),(17,'admin','0003_logentry_add_action_flag_choices','2020-01-23 06:09:37.904967'),(18,'sessions','0001_initial','2020-01-23 06:09:38.531927'),(19,'app','0002_auto_20200123_1254','2020-01-23 07:24:46.181867'),(20,'app','0003_loanmaster_customer','2020-01-23 12:21:17.402485'),(21,'app','0004_auto_20200123_1905','2020-01-23 13:35:22.262987');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('9z8oirmhlt5r2n8gv1kf7xtcheoqm83k','MDM3MTA0YzRkZjc4NzUwMTEwZGJiODU1MjU3OWQwNGUzYTExZDg4Yjp7Il9hdXRoX3VzZXJfaWQiOiJTVVBFUlVTRVIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6Ijc4NGE1NzgwZGIxNGQ1ZjMyYWExNjZjY2Q5ZDE4MTg5ZDQxZmQwNjMifQ==','2020-02-06 09:22:07.049498'),('s3h8v6afpli3c8n506dxhq6gxqcu2iis','MDM3MTA0YzRkZjc4NzUwMTEwZGJiODU1MjU3OWQwNGUzYTExZDg4Yjp7Il9hdXRoX3VzZXJfaWQiOiJTVVBFUlVTRVIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6Ijc4NGE1NzgwZGIxNGQ1ZjMyYWExNjZjY2Q5ZDE4MTg5ZDQxZmQwNjMifQ==','2020-02-06 06:15:48.464206');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-24 13:15:56
