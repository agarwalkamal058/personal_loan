# personal_loan

Python Django Based project

# API list
<ul>
    <li>BaseURL/admin/</li><br/>
    <li>BaseURL/health-check/</li><br/>
    <li>BaseURL/</li><br/>
    <li>BaseURL/pending-loan/</li><br/>
    <li>BaseURL/approved-loan/</li><br/>
    <li>BaseURL/rejected-loan/</li><br/>
    <li>BaseURL/approve/-id-/</li><br/>
    <li>BaseURL/reject/-id-/</li><br/>
    <li>BaseURL/register-customer/</li><br/>
    <li>BaseURL/submit-new-customer/</li><br/>
    <li>BaseURL/apply-loan/</li><br/>
    <li>BaseURL/submit-new-loan/</li><br/>
    <li>BaseURL/make-payment/</li><br/>
    <li>BaseURL/pay-amount/</li><br/>
    <li>BaseURL/details/-id-/</li><br/>
    <li>BaseURL/types/</li><br/>
    <li>BaseURL/payments/</li><br/>
    <li>BaseURL/employees/</li><br/>
    <li>BaseURL/company/</li><br/>
    <li>BaseURL/borrowers/</li><br/>
</ul><br />

# Database tables name
<table>
    <tr>
        <th>
            Tables_in_personal_loan
        </th>
    </tr>
    <tr>
        <td>
            app_customermaster
        </td>
    </tr>
    <tr>
        <td>
            app_designationmaster
        </td>
    </tr>
    <tr>
        <td>
            app_loanmaster
        </td>
    </tr>
    <tr>
        <td>
            app_loanschememaster
        </td>
    </tr>
    <tr>
        <td>
            app_paymentmaster
        </td>
    </tr>
    <tr>
        <td>
            app_usermaster
        </td>
    </tr>
    <tr>
        <td>
            app_usermaster_groups
        </td>
    </tr>
    <tr>
        <td>
            app_usermaster_user_permissions
        </td>
    </tr>
    <tr>
        <td>
            auth_group
        </td>
    </tr>
    <tr>
        <td>
            auth_group_permissions
        </td>
    </tr>
    <tr>
        <td>
            auth_permission
        </td>
    </tr>
    <tr>
        <td>
            django_admin_log
        </td>
    </tr>
    <tr>
        <td>
            django_content_type
        </td>
    </tr>
    <tr>
        <td>
            django_migrations
        </td>
    </tr>
    <tr>
        <td>
            django_session
        </td>
    </tr>
</table>


# Structure for every table
<strong>app_customermaster</strong>
<ul>
    <li>customer_id	int(11) AI PK</li>
    <li>customer_name	varchar(100)</li>
    <li>phone_number	bigint(20)</li>
    <li>email_id	varchar(100)</li>
    <li>dob	datetime(6)</li>
    <li>yearly_income	bigint(20)</li>
    <li>occupation	varchar(20)</li>
    <li>address	varchar(250)</li>
    <li>aadhaar_no	bigint(20)</li>
    <li>pancard_no	varchar(12)</li>
    <li>passport	varchar(20)</li>
    <li>created_on	datetime(6)</li>
    <li>updated_on	datetime(6)</li>
    <li>created_by_id	varchar(20)</li>
    <li>updated_by_id	varchar(20)</li>
</ul><br />
<strong>app_designationmaster</strong>
<ul>
    <li>designation	varchar(20) PK</li>
    <li>designation_name	varchar(30)</li>
    <li>priority	smallint(5) UN</li>
    <li>created_on	datetime(6)</li>
    <li>created_by	varchar(200)</li>

</ul><br />
<strong>app_loanmaster</strong>
<ul>
    <li>auto_display_id	int(11)</li>
    <li>loan_acc_no	varchar(50) PK</li>
    <li>loan_start_date	datetime(6)</li>
    <li>loan_end_date	datetime(6)</li>
    <li>loan_amount	double</li>
    <li>created_on	datetime(6)</li>
    <li>updated_on	datetime(6)</li>
    <li>created_by_id	varchar(20)</li>
    <li>loan_scheme_id	int(11)</li>
    <li>updated_by_id	varchar(20)</li>
    <li>loan_status	varchar(12)</li>

</ul><br />
<strong>app_loanschememaster</strong>
<ul>
    <li>loan_scheme_id	int(11) AI PK</li>
    <li>loan_type	varchar(20)</li>
    <li>content	longtext</li>
    <li>amount_range	varchar(50)</li>
    <li>scheme_years	smallint(5) UN</li>
    <li>scheme_months	smallint(5) UN</li>
    <li>interest_rate	double</li>
    <li>created_on	datetime(6)</li>
    <li>updated_on	datetime(6)</li>
    <li>created_by_id	varchar(20)</li>
    <li>updated_by_id	varchar(20)</li>

</ul><br />
<strong>app_paymentmaster</strong>
<ul>
    <li>payment_id	int(11) AI PK</li>
    <li>comment	varchar(120)</li>
    <li>amount	double</li>
    <li>payment_status	varchar(12)</li>
    <li>created_on	datetime(6)</li>
    <li>updated_on	datetime(6)</li>
    <li>loan_acc_no_id	varchar(50)</li>

</ul><br />
<strong>app_usermaster</strong>
<ul>
    <li>password	varchar(128)</li>
    <li>last_login	datetime(6)</li>
    <li>is_superuser	tinyint(1)</li>
    <li>username	varchar(150)</li>
    <li>first_name	varchar(30)</li>
    <li>last_name	varchar(150)</li>
    <li>email	varchar(254)</li>
    <li>is_staff	tinyint(1)</li>
    <li>date_joined	datetime(6)</li>
    <li>emp_id	varchar(20) PK</li>
    <li>emp_name	varchar(200)</li>
    <li>emp_phone_num	bigint(20)</li>
    <li>email_id	varchar(200)</li>
    <li>is_active	tinyint(1)</li>
    <li>created_on	datetime(6)</li>
    <li>created_by	varchar(200)</li>
    <li>updated_on	datetime(6)</li>
    <li>updated_by	varchar(200)</li>
    <li>designation_id	varchar(20)</li>
</ul><br />
