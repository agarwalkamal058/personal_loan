from django import forms
from .models import *


class NoValidationChoice(forms.ChoiceField):
    def validate(self, value):
        pass

class HomeForm(forms.Form):
    welcome_note= forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))

class RegisterCustomerForm(forms.Form):
    OCCUPATION_CHOICES= (
        ('BUSINESS', 'Business'),
        ('SERVICE', 'Service'),
        ('UNEMPLOYED', 'Un employed')
    )

    customer_name= forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))
    phone_number= forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))
    email_id= forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))
    dob= forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date', 'class': 'form-control form-control-sm', 'placeholder':'Date of Birth'}),required=False)
    yearly_income= forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'Enter Yearly income'}), required=True)
    occupation= forms.ChoiceField(choices=OCCUPATION_CHOICES, initial='yes', widget=forms.RadioSelect(attrs={'class': ''}))
    address= forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))
    aadhaar_no= forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'xx'}), required=True)
    pancard_no= forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))
    passport= forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))

    def __init__(self, *args, **kwargs):
        super(RegisterCustomerForm, self).__init__(*args, **kwargs)
        self.fields['phone_number'].label= 'Mobile Number'

class ApplyLoanForm(forms.Form):
    OCCUPATION_CHOICES= (
        ('BUSINESS', 'Business'),
        ('SERVICE', 'Service'),
        ('UNEMPLOYED', 'Un employed')
    )

    loan_scheme= forms.ChoiceField(choices=[(x.loan_scheme_id, x.loan_scheme_id) for x in LoanSchemeMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    customer= forms.ChoiceField(choices=[(x.customer_id, x.customer_name) for x in CustomerMaster.objects.all()], widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    loan_start_date= forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date', 'class': 'form-control form-control-sm'}),required=False)
    loan_end_date= forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date', 'class': 'form-control form-control-sm'}),required=False)
    loan_amount= forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'Enter Loan amount'}), required=True)
    
    def __init__(self, *args, **kwargs):
        super(ApplyLoanForm, self).__init__(*args, **kwargs)
        self.fields['loan_scheme'].label= 'Loan Scheme'
        self.fields['loan_start_date'].label= 'Loan Start Date'
        self.fields['loan_end_date'].label= 'Loan End Date'
        self.fields['loan_amount'].label= 'Loan Amount'

class PaymentForm(forms.Form):
    loan_acc_no= NoValidationChoice(widget=forms.Select(attrs={'class': 'form-control form-control-sm'}))
    amount= forms.CharField(widget=forms.TextInput(attrs={'type':'number','step':'.000001','min':'0', 'class': 'form-control form-control-sm', 'placeholder': 'Enter amount to be paid'}))
    comment= forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-sm'}))
    