from django.urls import path
from django.conf.urls import include, url
from . import views

urlpatterns = [
    path('health-check/', views.health_check_api, name='health_check_api'),
    url(r'^$', views.index_loan),
    url(r'^pending-loan/$', views.pending_loan, name='pending_loans'),
    url(r'^approved-loan/$', views.approved_loan, name='approved_loans'),
    url(r'^rejected-loan/$', views.rejected_loan, name='rejected_loans'),
    url(r'^approve/(?P<id>\d+)/$', views.approve, name='approve'),
    url(r'^reject/(?P<id>\d+)/$', views.reject, name='reject'),
    url(r'^register-customer/$', views.register_customer, name='register'),
    path('submit-new-customer/', views.save_customer, name='save_customer'),
    url(r'^apply-loan/$', views.apply_loan, name='loan'),
    path('submit-new-loan/', views.save_loan, name='save_loan'),
    url(r'^make-payment/$', views.show_make_payment, name='make_payment'),
    path('pay-amount/', views.pay_amount, name='pay_amount'),
    url(r'^details/(?P<id>\d+)/$', views.loan_details, name='details'),
    url(r'^types/$', views.loan_types, name='loantype'),
    url(r'^payments/$', views.payments, name = 'payment'),
    url(r'^employees/$', views.employees, name = 'employee'),
    url(r'^company/$', views.company_setup, name = 'company'),
    url(r'^borrowers/$', views.borrowers, name = 'borrower'),
    
]