from django.contrib import admin
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import *
import csv
from django import forms
from datetime import datetime
import traceback
from personal_loan import logger
log = logger.get_logger('admin')

'''
@author(Kamal Agarwal)
Generic CSV Export
'''


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = "Export Selected"


'''
@author(Kamal Agarwal)
Admin Filters
'''


class DesignationFilter(admin.SimpleListFilter):
    title = _('Designation')

    parameter_name = 'designation'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, j) for i, j in qs.values_list('designation__designation', 'designation__designation_name').distinct().order_by('designation__priority')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(designation__exact=self.value())


'''
@author(Kamal Agarwal)
Model Admins
'''


class MyGroupAdmin(GroupAdmin):
    def get_form(self, request, obj=None, **kwargs):
        # Get form from original GroupAdmin.
        form = super(MyGroupAdmin, self).get_form(request, obj, **kwargs)
        if 'permissions' in form.base_fields:
            permissions = form.base_fields['permissions']
            permissions.queryset = permissions.queryset.exclude(
                content_type__app_label__in=['admin', 'contenttypes', 'sessions']).exclude(codename__startswith='delete_')  # Example
        return form


class DesignationMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('designation', 'designation_name',
                    'priority', 'created_on')

    list_filter = ['created_on', 'priority']

    readonly_fields = ['created_on']

    ordering = ('priority',)

    actions = ["export_as_csv"]

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in DesignationMasterAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        return DesignationMaster.objects.all()

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(DesignationMasterAdmin,
                                self).get_readonly_fields(request, obj)
        if obj:  # editing an existing object
            return readonly_fields + ['designation', 'designation_name']
        return readonly_fields


class UserCreationFormExtended(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationFormExtended, self).__init__(*args, **kwargs)
        self.fields['email'] = forms.EmailField(
            label=("E-mail"), max_length=75)
        # self.fields['designation'] = forms.ChoiceField(
        #     choices=[(r.designation, r.designation) for r in DesignationMaster.objects.all()])


class UserChangeFormExtended(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(UserChangeFormExtended, self).__init__(*args, **kwargs)


class MyUserAdmin(UserAdmin):

    date_hierarchy = 'updated_on'

    form = UserChangeFormExtended
    fieldsets = (
        (None, {'fields': ('username', )}),
        (_('Personal info'), {'fields': ('emp_id', 'first_name',
                                         'last_name', 'email', 'emp_phone_num', 'designation', )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff',
                                       'groups', 'user_permissions')}),
        (_('Handled By'), {'fields': ('created_on',
                                      'created_by', 'updated_on', 'updated_by')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_form = UserCreationFormExtended
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('emp_id', 'email', 'emp_phone_num', 'first_name', 'last_name', 'designation', 'password1', 'password2', )
        }),
    )

    list_display = ('username', 'email', 'first_name',
                    'last_name', 'designation', 'is_staff',)
    list_filter = ('is_staff', 'is_active', 'groups', DesignationFilter, )
    readonly_fields = ('username', 'created_on', 'created_by',
                       'updated_on', 'updated_by', 'last_login', 'date_joined')
    search_fields = ('emp_id', 'emp_phone_num', 'first_name')
    ordering = ('designation__priority', '-updated_on',)

    def get_queryset(self, request):
        log.info("Enetered at get_queryset in MyUserAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        try:
            qs = UserMaster.objects.exclude(
                designation__priority__gte=(request.user.designation.priority))
        except Exception as e:
            log.error(
                "Exception occured at get_queryset in MyUserAdmin, Error msg: " + str(e))
            qs = UserMaster.objects.all()
        finally:
            return qs

    def get_actions(self, request):
        log.info("Enetered at get_actions in MyUserAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        actions = super().get_actions(request)
        try:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        except Exception as e:
            log.error(
                "Exception occured at get_actions in MyUserAdmin, Error msg: " + str(e))
        finally:
            return actions

    def get_form(self, request, obj=None, **kwargs):
        form = super(MyUserAdmin, self).get_form(request, obj, **kwargs)
        if 'user_permissions' in form.base_fields:
            user_permissions = form.base_fields['user_permissions']
            user_permissions.queryset = user_permissions.queryset.filter(codename__endswith='permission').exclude(content_type__app_label__in=['auth', 'admin', 'contenttypes', 'sessions'])
        return form

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        log.info("Enetered at save_model in MyUserAdmin, Logged-in user emp_id: " +
                 str(request.user.emp_id) + ".")
        obj.added_by = request.user
        obj.updated_by = request.user.emp_name
        obj.updated_on = datetime.now()
        user_obj = UserMaster.objects.get(username=request.user.username)
        if form.cleaned_data.get('emp_phone_num'):
            obj.username = form.cleaned_data.get('emp_phone_num')
        if form.cleaned_data.get('first_name') and form.cleaned_data.get('last_name'):
            obj.emp_name = str(
                form.cleaned_data.get('first_name') + " " + form.cleaned_data.get('last_name')).strip()
        obj.is_superuser = False
        obj.is_staff = True
        if not change:
            obj.created_by = request.user.emp_name
        log.info("Data to be eneterd/updated in UserMaster, Data are: emp_id::: " + str(obj.emp_id) + ", emp_name: " + str(obj.emp_name) +
                 ", emp_phone_num: " + str(obj.emp_phone_num) + ", designation: " + str(obj.designation) + ", username: " + str(obj.username))
        return super(MyUserAdmin, self).save_model(request, obj, form, change)


class CustomerMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('customer_id', 'customer_name', 'phone_number', 'email_id', 'dob', 'yearly_income', 'created_on')

    list_filter = ['created_on', 'updated_on']

    readonly_fields = ['created_on', 'updated_on']

    ordering = ('created_on',)

    actions = ["export_as_csv"]

    
class LoanMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('loan_acc_no', 'loan_scheme', 'loan_start_date', 'loan_end_date', 'loan_amount', 'created_on')

    list_filter = ['created_on', 'updated_on']

    readonly_fields = ['created_on', 'updated_on']

    ordering = ('created_on',)

    actions = ["export_as_csv"]

    
class LoanSchemeMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('loan_scheme_id', 'loan_type', 'amount_range', 'scheme_years', 'scheme_months', 'interest_rate', 'created_on')

    list_filter = ['created_on', 'updated_on', 'loan_type', 'amount_range']

    readonly_fields = ['created_on', 'updated_on']

    ordering = ('created_on',)

    actions = ["export_as_csv"]

    
class PaymentMasterAdmin(admin.ModelAdmin, ExportCsvMixin):

    date_hierarchy = 'created_on'

    list_display = ('payment_id', 'comment', 'amount', 'loan_acc_no', 'payment_status', 'created_on', 'updated_on')

    list_filter = ['created_on', 'updated_on', 'payment_status']

    readonly_fields = ['created_on', 'updated_on']

    ordering = ('created_on',)

    actions = ["export_as_csv"]

    
# Authentications and permissions register
admin.site.register(DesignationMaster, DesignationMasterAdmin)
admin.site.unregister(Group)
admin.site.register(Group, MyGroupAdmin)

admin.site.register(UserMaster, MyUserAdmin)
admin.site.register(CustomerMaster, CustomerMasterAdmin)
admin.site.register(LoanSchemeMaster, LoanSchemeMasterAdmin)
admin.site.register(LoanMaster, LoanMasterAdmin)
admin.site.register(PaymentMaster, PaymentMasterAdmin)
