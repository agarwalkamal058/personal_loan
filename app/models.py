from django.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator


class DesignationMaster(models.Model):
    designation = models.CharField(max_length=20, primary_key=True)
    designation_name = models.CharField(max_length=30, null=True)
    priority = models.PositiveSmallIntegerField(default=5, validators=[MinValueValidator(1), MaxValueValidator(10)])
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.CharField(max_length=200, default='SUPERUSER')

    def __str__(self):
        return str(self.designation_name)

    class Meta:
        verbose_name = 'Designation Master'
        verbose_name_plural = 'Designation Master'


class UserMaster(AbstractUser):
    emp_id = models.CharField(max_length=20, primary_key=True)
    emp_name = models.CharField(max_length=200)
    emp_phone_num = models.BigIntegerField(null=True, verbose_name='Mobile number', unique=True,)
    email_id = models.CharField(max_length=200, null=True, blank=True)
    designation = models.ForeignKey(DesignationMaster, default=None, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.CharField(max_length=200, default='SUPERUSER')
    updated_on = models.DateTimeField(default=timezone.now)
    updated_by = models.CharField(max_length=200, default='SUPERUSER')
    
    USERNAME_FIELD = 'emp_phone_num'

    def __str__(self):
        return str(self.emp_name)

    class Meta:
        verbose_name = 'User Master'
        verbose_name_plural = 'User Master'


class CustomerMaster(models.Model):
    BUSINESS, SERVICE, UNEMPLOYED = 'BUSINESS', 'SERVICE', 'UNEMPLOYED'

    OCCUPATION_CHOICES = (
        (BUSINESS, 'Business'),
        (SERVICE, 'Service'),
        (UNEMPLOYED, 'Un employed')
    )

    customer_id= models.AutoField(primary_key=True)
    customer_name= models.CharField(max_length=100)
    phone_number= models.BigIntegerField(verbose_name='Mobile number')
    email_id= models.CharField(max_length=100)
    dob= models.DateTimeField(null=True)
    yearly_income= models.BigIntegerField()
    occupation= models.CharField(default=UNEMPLOYED, choices=OCCUPATION_CHOICES, max_length=20)
    address= models.CharField(max_length=250, null=True)
    aadhaar_no= models.BigIntegerField(null=True)
    pancard_no= models.CharField(max_length=12, null=True)
    passport= models.CharField(max_length=20, null=True)
    created_on= models.DateTimeField(default=timezone.now)
    created_by= models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, related_name='CustomerMasterCreatedBy')
    updated_on= models.DateTimeField(default=timezone.now)
    updated_by= models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, related_name='CustomerMasterUpdatedBy')
    
    def __str__(self):
        return str(self.customer_id)

    class Meta:
        verbose_name = 'Customer Master'
        verbose_name_plural = 'Customer Master'

class LoanSchemeMaster(models.Model):
    PERSONAL = 'PERSONAL'

    TYPE_CHOICS = (
        (PERSONAL, 'Personal'), 
    )

    BELOWONELAKH, ONELAKHTOTOTENLAKH, TENLAKHTOFIFTYLAKH, FIFTYLAKHTOONECRORE, ONECROREPLUS = 'BELOWONELAKH', 'ONELAKHTOTOTENLAKH', 'TENLAKHTOFIFTYLAKH', 'FIFTYLAKHTOONECRORE', 'ONECROREPLUS'

    AMOUNT_CHOICES = (
        (BELOWONELAKH, 'Below One Lakh'), 
        (ONELAKHTOTOTENLAKH, 'One Lakh to Ten Lakh'), 
        (TENLAKHTOFIFTYLAKH, 'Ten Lakh to Fifty Lakh'), 
        (FIFTYLAKHTOONECRORE, 'Fifty Lakh to One Crore'), 
        (ONECROREPLUS, 'One Crore Plus'), 
    )

    loan_scheme_id= models.AutoField(primary_key=True)
    loan_type= models.CharField(default=PERSONAL, choices=TYPE_CHOICS, max_length=20)
    content = models.TextField()
    amount_range= models.CharField(max_length=50, default=BELOWONELAKH, choices=AMOUNT_CHOICES)
    scheme_years= models.PositiveSmallIntegerField(default=5, validators=[MinValueValidator(0), MaxValueValidator(50)])
    scheme_months= models.PositiveSmallIntegerField(default=5, validators=[MinValueValidator(0), MaxValueValidator(12)])
    interest_rate = models.FloatField(default=0)
    created_on= models.DateTimeField(default=timezone.now)
    created_by= models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, related_name='LoanSchemeMasterCreatedBy')
    updated_on= models.DateTimeField(default=timezone.now)
    updated_by= models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, related_name='LoanSchemeMasterUpdatedBy')
    
    def __str__(self):
        return str(self.loan_scheme_id)

    class Meta:
        verbose_name = 'Loan Scheme Master'
        verbose_name_plural = 'Loan Scheme Master'

class LoanMaster(models.Model):
    PENDING, APPROVED, REJECTED = 'PENDING', 'APPROVED', 'REJECTED'

    LOAN_STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (APPROVED, 'Approved'),
        (REJECTED, 'Rejected')
    )

    def increment_auto_display_number():
        last_auto_display = LoanMaster.objects.all().order_by('auto_display_id').last()
        if not last_auto_display:
            return 1
        old_auto_display_id = last_auto_display.auto_display_id
        new_auto_display_id = old_auto_display_id + 1
        return new_auto_display_id
    
    auto_display_id = models.IntegerField(default=increment_auto_display_number, editable=False)
    loan_acc_no = models.CharField(max_length=50, primary_key=True, editable=False)
    loan_scheme = models.ForeignKey(LoanSchemeMaster, on_delete=models.SET_NULL, null=True)
    customer = models.ForeignKey(CustomerMaster, on_delete=models.CASCADE, null=True)
    loan_start_date = models.DateTimeField(default=timezone.now)
    loan_end_date = models.DateTimeField(null=True)
    loan_amount= models.FloatField(default=0)
    loan_status = models.CharField(default=PENDING, choices=LOAN_STATUS_CHOICES, max_length=12)
    created_on= models.DateTimeField(default=timezone.now)
    created_by= models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, related_name='LoanMasterCreatedBy')
    updated_on= models.DateTimeField(default=timezone.now)
    updated_by= models.ForeignKey(UserMaster, on_delete=models.SET_NULL, null=True, related_name='LoanMasterUpdatedBy')
    
    def __str__(self):
        return str(self.loan_acc_no)

    def save(self, *args, **kwargs):
        if self._state.adding:
            last_loan_acc = self.__class__.objects.order_by('loan_acc_no').last()
            if last_loan_acc is not None:
                old_loan_acc_no = last_loan_acc.loan_acc_no
                old_loan_acc_no_int = int(str(old_loan_acc_no)[4:])
                new_loan_acc_no_int = old_loan_acc_no_int + 1
                self.loan_acc_no = 'ACLN' + str(new_loan_acc_no_int).zfill(10)
            else:
                self.loan_acc_no = 'ACLN' + '1000000001'
        super(LoanMaster, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Loan Master'
        verbose_name_plural = 'Loan Master'


class PaymentMaster(models.Model):
    PROCESSING, SUCCESS, FAILED, CANCELLED = 'PROCESSING', 'SUCCESS', 'FAILED', 'CANCELLED'

    PAYMENT_STATUS_CHOICES = (
        (PROCESSING, 'Processing'),
        (SUCCESS, 'Success'),
        (FAILED, 'Failed'), 
        (CANCELLED, 'Cancelled'), 
    )

    payment_id = models.AutoField(primary_key=True)
    comment = models.CharField(max_length=120)
    amount = models.FloatField(default=0)
    loan_acc_no = models.ForeignKey(LoanMaster, on_delete=models.CASCADE)
    payment_status = models.CharField(default=SUCCESS, choices=PAYMENT_STATUS_CHOICES, max_length=12)
    created_on = models.DateTimeField(auto_now = False, auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True, auto_now_add = False)
    
    def __str__(self):
        return str(self.payment_id)

    class Meta:
        verbose_name = 'Payment Master'
        verbose_name_plural = 'Payment Master'