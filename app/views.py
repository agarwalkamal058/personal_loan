from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from app.forms import *
from app.models import CustomerMaster, LoanMaster
from django.shortcuts import redirect
import traceback
from personal_loan import logger
log = logger.get_logger('app')


@csrf_exempt
def health_check_api(request):
    log.info("health_check_api, Request " + str(request))
    data = {
        "info": "Health checked successfully.",
        "status": True,
        "status_code": 200,
        "success": "SUCCESS",
        "msg": "Health checked successfully."
    }
    return JsonResponse(data)


@csrf_exempt
def save_customer(request):
    log.info("save_customer of views.")
    form = RegisterCustomerForm(request.POST or None)
    if form.is_valid():
        try:
            customerMaster_object = CustomerMaster.objects.create(
                customer_name=form.cleaned_data['customer_name'],
                phone_number=form.cleaned_data['phone_number'],
                email_id=form.cleaned_data['email_id'],
                dob=form.cleaned_data['dob'],
                yearly_income=form.cleaned_data['yearly_income'],
                occupation=form.cleaned_data['occupation'],
                address=form.cleaned_data['address'],
                aadhaar_no=form.cleaned_data['aadhaar_no'],
                pancard_no=form.cleaned_data['pancard_no'],
                passport=form.cleaned_data['passport'])
            log.info("Customer data created in CustomerMaster for customer name: " +
                     str(form.cleaned_data['customer_name']))
        except Exception as ex:
            log.error(
                "Exception occured while save customer details, Error msg: " + str(ex))
            return redirect('/register-customer/')
        log.info("Successfully returned to thanks page.")
        return render(request, 'thanks.html')
    log.error("Form Validation Failed for save_customer of views.")
    return redirect('/register-customer/')


@csrf_exempt
def save_loan(request):
    log.info("save_loan of views.")
    form = ApplyLoanForm(request.POST or None)
    if form.is_valid():
        try:
            loanMasterObject = LoanMaster.objects.create(
                loan_scheme_id=form.cleaned_data['loan_scheme'],
                customer_id=form.cleaned_data['customer'],
                loan_start_date=form.cleaned_data['loan_start_date'],
                loan_end_date=form.cleaned_data['loan_end_date'],
                loan_amount=form.cleaned_data['loan_amount'])
            log.info("Loan data created in LoanMaster for customer id: " +
                     str(form.cleaned_data['customer']))
        except Exception as ex:
            log.error(
                "Exception occured while save loan details, Error msg: " + str(ex))
            return redirect('/apply-loan/')
        log.info("Successfully returned to thanks page.")
        return render(request, 'success_loan_applied.html')
    log.error("Form Validation Failed for save_loan of views.")
    return redirect('/apply-loan/')


@csrf_exempt
def pay_amount(request):
    log.info("pay_amount of views.")
    form = PaymentForm(request.POST or None)
    if form.is_valid():
        try:
            customerMasterObject = PaymentMaster.objects.create(
                comment=form.cleaned_data['comment'],
                amount=form.cleaned_data['amount'],
                loan_acc_no_id=form.cleaned_data['loan_acc_no'])
            log.info("Payment data created in PaymentMaster against Loan acc no: " +
                     str(form.cleaned_data['loan_acc_no']))
        except Exception as ex:
            log.error(
                "Exception occured while save payment details, Error msg: " + str(ex))
            return redirect('/make-payment/')
        log.info("Successfully returned to thanks page.")
        return render(request, 'success_payment.html')
    log.error("Form Validation Failed for pay_amount of views.")
    return redirect('/make-payment/')


def index_loan(request):
    log.info("index_loan of views.")
    context = {
        "title": "Homepage"
    }
    return render(request, "index.html", context)


def pending_loan(request):
    log.info("pending_loan of views.")
    queryset = LoanMaster.objects.filter(loan_status=LoanMaster.PENDING).all()
    context = {
        "title": "Pending Loans",
        "LoanMaster_list": queryset
    }
    return render(request, "pending.html", context)


def approved_loan(request):
    log.info("approved_loan of views.")
    queryset = LoanMaster.objects.filter(loan_status=LoanMaster.APPROVED).all()
    context = {
        "title": "Approved Loans",
        "LoanMaster_list": queryset
    }
    return render(request, "approved.html", context)


def rejected_loan(request):
    log.info("rejected_loan of views.")
    queryset = LoanMaster.objects.filter(loan_status=LoanMaster.REJECTED).all()
    context = {
        "title": "Rejected Loans",
        "LoanMaster_list": queryset
    }
    return render(request, "rejected.html", context)


def approve(request, id=None):
    log.info("approve of views.")
    instance = get_object_or_404(LoanMaster, auto_display_id=id)
    instance.loan_status = LoanMaster.APPROVED
    instance.save()
    return redirect('/')


def reject(request, id=None):
    log.info("reject of views.")
    instance = get_object_or_404(LoanMaster, auto_display_id=id)
    instance.loan_status = LoanMaster.REJECTED
    instance.save()
    return redirect('/')


def register_customer(request):
    log.info("register_customer of views.")
    form = RegisterCustomerForm(request.POST or None)
    context = {
        "form": form,
        "title": "Register"
    }
    return render(request, "registercustomer.html", context)


def apply_loan(request):
    log.info("apply_loan of views.")
    form = ApplyLoanForm(request.POST or None)
    context = {
        "form": form,
        "title": "Apply"
    }
    return render(request, "apply.html", context)


def loan_details(request, id=None):
    log.info("loan_details of views.")
    instance = get_object_or_404(LoanMaster, auto_display_id=id)
    context = {
        "title": instance.loan_acc_no,
        "instance": instance
    }
    return render(request, "detail.html", context)


def loan_types(request):
    log.info("loan_types of views.")
    instance = LoanSchemeMaster.objects.values_list('loan_type').distinct()
    context = {
        "title": "Types of loans",
        "loan_types": instance
    }
    return render(request, "loantypes.html", context)


def show_make_payment(request):
    log.info("show_make_payment of views.")
    loanMasterObject = LoanMaster.objects.filter(loan_status=LoanMaster.APPROVED)
    loan_acc_no_list = [("", "--SELECT--")]
    for x in loanMasterObject:
        loan_acc_no_list.append((x.loan_acc_no, x.loan_acc_no))
    form = PaymentForm(request.POST or None)
    form.fields['loan_acc_no'].choices = loan_acc_no_list
    context = {
        "form": form,
        "title": "Make Payment"
    }
    return render(request, "makepayment.html", context)


def payments(request):
    log.info("payments of views.")
    money = PaymentMaster.objects.all()
    context = {
        "title": "Payments",
        "money": money
    }
    return render(request, "payments.html", context)


def employees(request):
    log.info("employees of views.")
    employees_obj = UserMaster.objects.all()
    context = {
        "title": "Employees",
        "employees": employees_obj
    }
    return render(request, "employees.html", context)


def company_setup(request):
    log.info("company_setup of views.")
    context = {
        "title": "Company setup",
        "aboutus": "This company provides Personal Loan"
    }
    return render(request, "company.html", context)


def borrowers(request):
    log.info("borrowers of views.")
    customers = CustomerMaster.objects.all()
    context = {
        "title": "Borrowers",
        "customers": customers
    }
    return render(request, "borrowers.html", context)
