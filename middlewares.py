
from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import render
from app.models import UserMaster
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse


class ExceptionMiddleware(MiddlewareMixin):

    def process_exception(self, request, exception):
        print("Entered at process_exception in ExceptionMiddleware, Exception occured of type " + str(type(exception)) + ", Exception msg: " + str(exception) + ".")
        import traceback
        print(traceback.format_exc())
        data = {
            "info": "Something went wrong, Please try again later.",
            "status": False,
            "status_code": 500,
            "success": "FAILURE",
            "msg": "Exception occured, Error msg: " + str(exception) + "."
        }
        return render(request, 'notfound.html', {'json_msg': data})